# README #

## What is this repository for?

Example project based on https://axelfontaine.com/blog/dead-burried.html

Supports the Spring versioning model:

* 1.0.0.BUILD-SNAPSHOT
* 1.0.0.M1 
* 1.0.0.RC1 
* 1.0.0.RELEASE 

or using a build number 1.0.0.nnn

### Local/Snapshot builds
mvn deploy	=> 1.0.0.BUILD-SNAPSHOT

### Milestone builds
mvn -Prelease deploy scm:tag -Drevision=.M1	=> 1.0.0.M1
mvn -Prelease deploy scm:tag -Drevision=.RC1	=> 1.0.0.RC1

### Release builds
mvn -Prelease deploy scm:tag	=> 1.0.0.RELEASE

### Continuous delivery release builds
mvn -Prelease deploy scm:tag -Drevision=-${BUILD_NUMBER}	=> 1.0.0.23
